package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        String[] arrFruits = {"apple","avocado","banana","kiwi", "orange"};
        System.out.println("Fruits in Stock: " + Arrays.toString(arrFruits));
        Scanner fruitIndex = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of:");
        String myFruit = fruitIndex.nextLine();
        int len = arrFruits.length;
        int count = 0;

        while(count < len){
            if(myFruit.equals(arrFruits[count])){
                System.out.println("The index of "+myFruit+" is: "+count);
                break;
            }
            else{
                count = count + 1;
            }
        }
        ArrayList<String> friends = new ArrayList<>();
        friends.add("Paul");
        friends.add("John");
        friends.add("Maria");
        friends.add("Jane");
        System.out.println("My friends are: "+friends);

        HashMap<String, Integer> products = new HashMap<>();
        products.put("toothpaste",15);
        products.put("toothbrush",20);
        products.put("soap",12);
        System.out.println("Our supplies consist of: " + products);


    }
}
