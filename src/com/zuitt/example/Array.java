package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Array {
    /*
     Arrays in JAva
     In java, arrays are significantly rigid, even before filing the arrays should already identify teh data type and the size of the array
    Syntax:
        dataType[] identifier = new dataType[number of elements];

        dataType[] identifier = {elementA, elementB, elementC....};
     */
    public static void main(String[] args){
        String[] newArr = new String[3];
        System.out.println(Arrays.toString(newArr));
        newArr[0]="Clark";
        System.out.println(Arrays.toString(newArr));
        newArr[1]="Bruce";
        newArr[2]="Lois";
        System.out.println(Arrays.toString(newArr));

        String[] arrSample = {"Tony","Steve","Thor",""};
        System.out.println(Arrays.toString(arrSample));
        arrSample[3]="Peter";
        System.out.println(Arrays.toString(arrSample));

        Integer[] intArr = new Integer[5];
        intArr[0] = 21;
        intArr[1] = 55;
        intArr[2] = 32;
        intArr[3] = 12;
        intArr[4] = 68;
        System.out.println("Initial order of intArr:" + Arrays.toString(intArr));
        Arrays.sort(intArr);
        System.out.println("Order of items after sort:" + Arrays.toString(intArr));

        Arrays.sort(intArr, Collections.reverseOrder());
        System.out.println("Order of items after sort:" + Arrays.toString(intArr));

//        ArrayList
        /*
        * Array lists are resizable collections/ arrays that function similarly to how arrays work in JS
        *
        * Syntax:
        *   ArrayList<dataType> identifiers = new ArrayList<>();
        * */

        ArrayList<String> students = new ArrayList<>();
        // Array lists methods
        // arrayListName.add(itemToAdd) - adds elements in our array list

        students.add("Paul");
        students.add("John");
        students.add("Maria");
        students.add("Jane");
        System.out.println(students);


        //        arrayListName.get(index) - retrieve items from the array list
        System.out.println(students.get(3));

        //        arrayListName.set(index, value) - update an item by its index
        students.set(1,"George");
        System.out.println(students);

        //        arrayListName.remove(index) - removes an item by its index
        students.remove(1);
        System.out.println(students);

//        arrayListName.clear() - clears out items in the array list
        students.clear();
        System.out.println(students);

//        arrayListName.size() - gets length of the array
        students.add("Melissa");
        students.add("Melanie");
        students.add("Rowena");
        students.add("Florame");
        System.out.println(students.size());

        //ArrayList with initialized values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill","Elon","Jeff"));
        System.out.println(employees);
        employees.add("Tina");
        System.out.println(employees);

//        add(index, elementToBeAdded);
        employees.add(2,"Ashley");
        System.out.println(employees);

//    Hashnaps
//        most objects in java are defined and are instantiations of Classes  that contain a proper set of properties are methods. There are might be used cases where is this not appropriate. or you may simply want to store a collection of data in key-value pairs
//        in java "keys" also referred as "fields"
//        Syntax: HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();
        HashMap<String, String> userRoles = new HashMap<>();
//        add new fields and values in the hashmap
//        hashMapName.put(<field>,<value>);
        userRoles.put("Anna","Admin");
        userRoles.put("Alice", "Mage");
        System.out.println(userRoles);
        userRoles.put("Alice", "Marksmen");
        System.out.println(userRoles);
        userRoles.put("Dennis", "Tank");
        System.out.println(userRoles);

//        retrieve values by fields
//        hashMapName.get("field")

        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("Tank"));

// removes an element / field-value
//        hashMapName.remove("field");
        userRoles.remove("Anna");
        System.out.println(userRoles);

//        retrieve hashMap keys
//        hashMapName.keySet();
        System.out.println(userRoles.keySet());

    }
}
